#!/usr/bin/env python

import roslib; roslib.load_manifest('grasps_detection_cnn_gd_pkg')
import rospy
#from grasps_detection_cnn_gd_pkg.msg import visual_info, hand_config
from grasps_detection_cnn_gd_pkg.srv import *
from grasps_detection_cnn_gd_pkg.msg import *
import tf
from sensor_msgs.msg import Image, JointState, CameraInfo
#import sensor_msgs.point_cloud2 as pc2
from geometry_msgs.msg import Pose, Quaternion, PoseStamped
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np
from gen_rgbd_images import GenRgbdImage

import roslib.packages as rp
import sys
sys.path.append(rp.get_pkg_dir('grasp_data_collection_pkg') 
        + '/src')
from compute_finger_tip_location import ComputeFingerTipPose
#from grasp_rgbd_net import GraspRgbdNet
#from grasp_rgbd_inf import GraspRgbdInf
from grasp_rgbd_inf import GraspInf
import time

#def convert_full_to_preshape_config(hand_config):
#    '''
#    Convert full grasp configuration to preshape grasp configuration by deleting uninferred joint
#    angles.
#    '''
#    palm_quaternion = (hand_config.palm_pose.pose.orientation.x, hand_config.palm_pose.pose.orientation.y,
#            hand_config.palm_pose.pose.orientation.z, hand_config.palm_pose.pose.orientation.w) 
#    palm_euler = tf.transformations.euler_from_quaternion(palm_quaternion)
#
#    preshape_config = [hand_config.palm_pose.pose.position.x, hand_config.palm_pose.pose.position.y,
#            hand_config.palm_pose.pose.position.z, palm_euler[0], palm_euler[1], palm_euler[2],
#            hand_config.hand_joint_state.position[0], hand_config.hand_joint_state.position[1],
#            hand_config.hand_joint_state.position[4], hand_config.hand_joint_state.position[5],
#            hand_config.hand_joint_state.position[8], hand_config.hand_joint_state.position[9],
#            hand_config.hand_joint_state.position[12], hand_config.hand_joint_state.position[13]]
#
#    return np.array(preshape_config)

class GraspsInferenceServer:
    def __init__(self):
        rospy.init_node('grasps_inference_server')
        self.compute_finger_tip_loc = ComputeFingerTipPose()
        self.rgbd_patches_save_path = rospy.get_param('~data_recording_path', 
                '/media/kai/multi_finger_sim_data/')
        self.use_hd = rospy.get_param('~use_hd', True)
        self.gen_rgbd = GenRgbdImage(self.rgbd_patches_save_path) 
        #self.grasp_rgbd_inf = GraspRgbdInf(use_hd=self.use_hd)
        self.grasp_rgbd_inf = GraspInf(config_net=True, use_hd=self.use_hd)
        
    def handle_grasps_inference(self, req):
        '''
            Grasps inference service handler. 
        '''
        rgbd_image = self.gen_rgbd.get_rgbd_image(req.rgb_image_path, req.depth_image_path, 
                                                        req.rgbd_info.scene_cloud_normal) 

        self.gen_rgbd.save_rgbd_into_h5(rgbd_image, req.init_hand_pcd_true_config, req.init_hand_pcd_goal_config, 
                                        req.close_hand_pcd_config, object_id=req.object_id, grasp_id=req.grasp_id,
                                        object_name=req.object_name)

        self.compute_finger_tip_loc.set_up_input(req.init_hand_pcd_goal_config.palm_pose, 
                                                    req.init_hand_pcd_goal_config.hand_joint_state, use_hd=self.use_hd)
        self.compute_finger_tip_loc.proj_finger_palm_locs_to_img()
        palm_image_loc = self.compute_finger_tip_loc.palm_image_loc
        #finger_tip_image_locs = self.compute_finger_tip_loc.finger_tip_image_locs 
        #rospy.loginfo(palm_image_loc)
        #rospy.loginfo(finger_tip_image_locs)
        #palm_patch, finger_tip_patches = self.gen_rgbd.get_finger_palm_patches(rgbd_image, palm_image_loc, finger_tip_image_locs, 
        #                                        object_id=req.object_id, grasp_id=req.grasp_id, 
        #                                        object_name=req.object_name, grasp_label=req.grasp_success_label,
        #                                        save=True)

        #self.gen_rgbd.get_finger_palm_no_rot_patches(rgbd_image, palm_image_loc, finger_tip_image_locs, 
        #                                        object_id=req.object_id, grasp_id=req.grasp_id, 
        #                                        object_name=req.object_name, grasp_label=req.grasp_success_label,
        #                                        save=False)

        #self.compute_finger_tip_loc.set_up_input(req.close_hand_pcd_config.palm_pose, 
        #                                        req.close_hand_pcd_config.hand_joint_state)
        #self.compute_finger_tip_loc.proj_finger_palm_locs_to_img()
        #close_palm_image_loc = self.compute_finger_tip_loc.palm_image_loc
        #close_finger_image_loc = self.compute_finger_tip_loc.finger_tip_image_locs 

        #palm_patch, finger_tip_patches = self.gen_rgbd.get_finger_palm_patches(rgbd_image, close_palm_image_loc, close_finger_image_loc, 
        #                                        close_hand=True, object_id=req.object_id, grasp_id=req.grasp_id, 
        #                                        object_name=req.object_name, grasp_label=req.grasp_success_label,
        #                                        save=True)

        #self.gen_rgbd.get_finger_palm_no_rot_patches(rgbd_image, close_palm_image_loc, close_finger_image_loc, 
        #                                        close_hand=True, object_id=req.object_id, grasp_id=req.grasp_id, 
        #                                        object_name=req.object_name, grasp_label=req.grasp_success_label,
        #                                        save=False)

        #preshape_true_config = convert_full_to_preshape_config(req.init_hand_pcd_true_config) 
        #preshape_goal_config = convert_full_to_preshape_config(req.init_hand_pcd_goal_config) 
        #close_shape_config = convert_full_to_preshape_config(req.close_hand_pcd_config) 
        #rospy.loginfo(preshape_true_config)
        #rospy.loginfo(preshape_goal_config)
        #rospy.loginfo(close_shape_config)
        #grasp_patch = self.gen_rgbd.get_grasp_patch(rgbd_image, palm_image_loc, close_palm_image_loc, preshape_true_config, 
        #                                            preshape_goal_config, close_shape_config, object_id=req.object_id,
        #                                            grasp_id=req.grasp_id, object_name=req.object_name, 
        #                                            grasp_label=req.grasp_success_label, save=True)

        grasp_patch = self.gen_rgbd.extract_rgbd_patch(rgbd_image, tuple(palm_image_loc), patch_size=400)  

        #grasp_rgbd_net = GraspRgbdNet()
        #grasp_rgbd_net.init_net_inf()
        #d_prob_d_palm, d_prob_d_fingers, suc_prob = \
        #        grasp_rgbd_net.get_rgbd_gradients(palm_patch, finger_tip_patches)
        #d_prob_d_fingers = np.array(d_prob_d_fingers)
        #print d_prob_d_palm.shape, d_prob_d_fingers.shape, suc_prob.shape
        #print np.mean(d_prob_d_palm), np.mean(d_prob_d_fingers), np.mean(suc_prob)

        #config_opt, suc_prob, suc_prob_init = \
        #        self.grasp_rgbd_inf.gradient_descent_inf(rgbd_image, req.init_hand_pcd_config, 
        #                                    save_grad_to_log=True, object_id=req.object_id, grasp_id=req.grasp_id) 
        
        #config_opt, suc_prob, suc_prob_init = \
        #        self.grasp_rgbd_inf.quasi_newton_lbfgs_inf(rgbd_image, req.init_hand_pcd_config) 

        response = GraspInferenceResponse()
        config_opt, suc_prob, suc_prob_init = \
                self.grasp_rgbd_inf.gradient_descent_inf(grasp_patch, req.init_hand_pcd_goal_config, 
                                            save_grad_to_log=False, object_id=req.object_id, 
                                            grasp_id=req.grasp_id) 
        #print req.init_hand_pcd_goal_config
        #print config_opt, suc_prob, suc_prob_init 
        print 'top grasp:', req.is_top_grasp
        print suc_prob, suc_prob_init 

        response.inf_hand_pcd_config = config_opt
        response.inf_suc_prob = suc_prob
        response.init_suc_prob = suc_prob_init
        response.success = True
        return response

    def create_grasps_inference_server(self):
        '''
            Create grasps inference server.
        '''
        rospy.Service('grasps_inference', GraspInference, self.handle_grasps_inference)
        #rospy.Service('grasps_inference', GraspDataRecording, self.handle_grasps_inference)
        rospy.loginfo('Service grasps_inference:')
        rospy.loginfo('Ready to infer grasps.')

    def handle_grasps_sample_inference(self, req):
        '''
            Grasps inference service handler. 
        '''
        rgbd_image = self.gen_rgbd.get_rgbd_image(req.rgb_image_path, req.depth_image_path, 
                                                        req.rgbd_info.scene_cloud_normal) 
        hand_config_dummy = HandConfig() 
        self.gen_rgbd.save_rgbd_into_h5(rgbd_image, hand_config_dummy, hand_config_dummy, 
                                        hand_config_dummy, object_id=req.object_id, grasp_id=req.grasp_id,
                                        object_name=req.object_name)
       
        t = time.time()
        max_suc_prob = -1.
        max_suc_prob_idx = -1
        for i in xrange(len(req.sample_hand_joint_state_list)):
            sample_hand_config = HandConfig()
            sample_hand_config.palm_pose = req.sample_palm_pose_list[i]
            sample_hand_config.hand_joint_state = req.sample_hand_joint_state_list[i]
            self.compute_finger_tip_loc.set_up_input(sample_hand_config.palm_pose, 
                                                        sample_hand_config.hand_joint_state, use_hd=self.use_hd)
            self.compute_finger_tip_loc.proj_finger_palm_locs_to_img()
            palm_image_loc = self.compute_finger_tip_loc.palm_image_loc
            grasp_patch = self.gen_rgbd.extract_rgbd_patch(rgbd_image, tuple(palm_image_loc), patch_size=400)  
            q = self.grasp_rgbd_inf.convert_full_to_preshape_config(sample_hand_config)
            suc_prob = self.grasp_rgbd_inf.get_config_suc_prob_config_net(q, grasp_patch)
            if suc_prob >= max_suc_prob:
                max_suc_prob = suc_prob
                max_suc_prob_idx = i

        elapased_time = time.time() - t
        print 'Total sample inference time: ', str(elapased_time)
        response = GraspSampleInfResponse()
        response.max_suc_prob = max_suc_prob
        response.max_suc_prob_sample_idx = max_suc_prob_idx
        response.success = True
        print response
        return response

    def create_grasps_sample_inf_server(self):
        '''
            Create grasps inference server.
        '''
        rospy.Service('grasps_sample_inference', GraspSampleInf, self.handle_grasps_sample_inference)
        rospy.loginfo('Service grasps_sample_inference:')
        rospy.loginfo('Ready to evaluated sampled grasps.')



if __name__ == '__main__':
    grasps_inference = GraspsInferenceServer() 
    grasps_inference.create_grasps_inference_server()
    grasps_inference.create_grasps_sample_inf_server()
    rospy.spin()

